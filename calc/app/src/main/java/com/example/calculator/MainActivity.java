package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Text1 = findViewById(R.id.textView1);
        Button1 = findViewById(R.id.button1);
        Button2 = findViewById(R.id.button2);
        Button3 = findViewById(R.id.button3);
        Button4 = findViewById(R.id.button4);
        Button5 = findViewById(R.id.button5);
        Button6 = findViewById(R.id.button6);
        Button7 = findViewById(R.id.button7);
        Button8 = findViewById(R.id.button8);
        Button9 = findViewById(R.id.button9);
        Button10 = findViewById(R.id.button10);
        Button11 = findViewById(R.id.button11);
        Button12 = findViewById(R.id.button12);
        Button13 = findViewById(R.id.button13);
        Button14 = findViewById(R.id.button14);
        Button15 = findViewById(R.id.button15);
        Button16 = findViewById(R.id.button16);
        Button17 = findViewById(R.id.button15);
        Button18 = findViewById(R.id.button14);
        Button19 = findViewById(R.id.button13);
        sin_button = findViewById(R.id.sin_button);

    }
    TextView Text1;
    Button Button1;
    Button Button2;
    Button Button3;
    Button Button4;
    Button Button5;
    Button Button6;
    Button Button7;
    Button Button8;
    Button Button9;
    Button Button10;
    Button Button11;
    Button Button12;
    Button Button13;
    Button Button14;
    Button Button15;
    Button Button16;
    Button Button17;
    Button Button18;
    Button Button19;
    Button sin_button;
    Double i;
    Double addAct;
    Double result;
    String Action;


    public void Button1Click(View view) {

       checkZero(Button1.getText().toString());

    }

    public void Button2Click(View view) {
        checkZero(Button2.getText().toString());
    }

    public void Button3Click(View view) {
        mathActions(Button3.getText().toString());

    }

    public void Button4Click(View view) {
        mathActions(Button4.getText().toString());
    }
    public void checkZero(String value) {
        if (Text1.getText().equals("0")) {
            Text1.setText(value);
        }
        else {
            Text1.setText(Text1.getText().toString() + value);
        }
    }
    public void Button5Click(View view) {
        checkZero(Button5.getText().toString());
    }

    public void Button6Click(View view) {
        checkZero(Button6.getText().toString());
    }

    public void Button7Click(View view) {
        checkZero(Button7.getText().toString());
    }

    public void Button8Click(View view) {
        mathActions(Button8.getText().toString());

    }

    public void setI() {
        i = Double.valueOf(Text1.getText().toString());
        Text1.setText("0");
    }

    public void setIForAddActions() {
        addAct = Double.valueOf(Text1.getText().toString());
    }

    public void mathActions(String action) {
        try{
            if (!Text1.getText().equals("0"))
            {
                switch(action) {
                    case "+": Action = "+";
                        setI();
                        break;
                    case "*": Action = "*";
                        setI();
                        break;
                    case "=": checkAction(Action);
                        break;
                    case "/": Action = "/";
                        setI();
                        break;
                    case "-": Action = "-";
                        setI();
                        break;
                    case "111": Action = "111";
                        setIForAddActions();
                        break;
                    case "222": Action = "222";
                        setIForAddActions();
                        break;
                    case "333": Action = "333";
                        setIForAddActions();
                        break;
                        default: Text1.setText("error");

                }

            }

        }
        catch (Exception ex)
        {
            Text1.setText(ex.getMessage());
        }
    }

    public void  checkAction (String action) {
        switch(action) {
            case "+":
                result = Double.valueOf(i) + Double.valueOf(Text1.getText().toString());
                Text1.setText(result.toString());
                break;
            case "*":
                result = Double.valueOf(i) * Double.valueOf(Text1.getText().toString());
                Text1.setText(result.toString());
                break;
            case "/":
                result = Double.valueOf(i) / Double.valueOf(Text1.getText().toString());
                Text1.setText(result.toString());
                break;
            case "-":
                result = Double.valueOf(i) - Double.valueOf(Text1.getText().toString());
                Text1.setText(result.toString());
                break;
            case "111":
                result = Double.valueOf(Math.sin(addAct));
                Text1.setText(result.toString());
                break;
            case "222":
                result = Double.valueOf(Math.cos(addAct));
                Text1.setText(result.toString());
            case "333":
                result = Double.valueOf(Math.tan(addAct));
                Text1.setText(result.toString());
                break;
                default: setTitle("fgfgfg");
        }
    }

    public void Button9Click(View view) {
        checkZero(Button9.getText().toString());
    }

    public void Button12Click(View view) {
        mathActions(Button12.getText().toString());
    }

    public void Button11Click(View view) {
        checkZero(Button11.getText().toString());
    }

    public void Button10Click(View view) {
        checkZero(Button10.getText().toString());
    }

    public void Button16Click(View view) {
        mathActions(Button16.getText().toString());
    }

    public void Button14Click(View view) {
        checkZero(Button14.getText().toString());
    }

    public void Button13Click(View view) {
        checkZero(Button13.getText().toString());
    }

    public void Button15Click(View view) {
        Text1.setText("0");
        result = 0.0;
    }

    public void Button17Click(View view) {
        if(Text1.getText().toString().contains("."))
        {

        }
        else
        {
            Text1.setText(Text1.getText() + ".");
        }
    }

    public void sin_button(View view) {
        mathActions("111");
    }

    public void cos_button(View view) {
        mathActions("222");
    }

    public void tan_button(View view) {
        mathActions("333");
    }
}